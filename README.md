![Build Status](https://gitlab.com/EAL-ITT/20a-itt3-pcb/badges/master/pipeline.svg)


# 20A-ITT3-PCB

weekly plans, resources and other relevant stuff

public website for students:

*  [https://eal-itt.gitlab.io/20a-itt3-pcb/](https://eal-itt.gitlab.io/20a-itt3-pcb/)

students gitlab group:

* [https://gitlab.com/20a-itt3-pcb-students-group](https://gitlab.com/20a-itt3-pcb-students-group)