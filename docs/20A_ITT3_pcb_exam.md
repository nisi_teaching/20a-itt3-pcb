---
title: '20A ITT3 PCB'
subtitle: 'Exam description'
filename: '20A_ITT3_pcb_exam'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2020-10-06
email: 'nisi@ucl.dk'
left-header: \today
right-header: Links
skip-toc: false
---

# Document content

This document describes the exam and hand-in requirements for the PCB elective course.

# Exam description

The exam is individual.  

The exam is a poster presentation where students present the process of designing their
PCB as well as a presentation of the physical product (Populated PCB).  
The entire exam has a duration of 2,5 hours where students will present to anyone interested.  
During the exam students will present to a lecturer for maximum 10 minutes.  
 
The exam takes place at Seebladsgade 1, 5000 Odense.  
Room number is announced at Wiseflow

There will be cardboard walls available for the posters.   

# Grading

Grading is based on a combination of the poster presentation and the report.  
Each student is graded according to the fullfillment of the PCB course learning goals described in the [course material](https://eal-itt.gitlab.io/20a-itt3-pcb/20A_ITT3_PCB_weekly_plans.pdf)  

Grading is 7 grade scale.  
The exam is with internal sensor. 

# Hand-in requirements

The hand-in is individual.  

Students are expected to hand-in a report on wiseflow.   

Page count: Minimum 5 and maximum 10 standard pages.  
*The title page, list of contents, bibliography and annexes do not count towards the required number of pages. Annexes are not assessed.*  
*A standard page means 2,400/x characters including spaces and footnotes.*  

Minimum content requirements:

* Front page with picture of finished PCB
* A description of the PCB production process
* Link to gitlab repository

# Exam dates

1st attempt **October 21st 12:30 – 15:00**    
2nd attempt **November 3rd 2020 12:30 - 15:00**  
3rd attempt **November 17th 2020 12:30 - 15:00**  

# Hand-in dates

1st attempt **October 20th 2020 17:00**  
2nd attempt **November 2nd 2020 17:00**  
3rd attempt **November 16th 2020 17:00**  

# Additional information

See the [semester description](https://esdhweb.ucl.dk/D20-1394523.pdf?nocache=8ec27635-588b-4367-93c7-3b99fd3c0d29&_ga=2.266824398.1784206826.1601976395-975700373.1599562108)