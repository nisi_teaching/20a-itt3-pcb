---
title: '20A ITT3 PCB Links'
subtitle: 'Links'
filename: 'links'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2020-06-15
email: 'nisi@ucl.dk'
left-header: \today
right-header: Links
skip-toc: false
---

# Links

Below are useful links to standards, legislation and other PCB related ressources.

## Standards

IPC standards [http://www.hytekaalborg.dk/da/ipc-standarder](http://www.hytekaalborg.dk/da/ipc-standarder)  
Perfag standards [http://www.perfag.dk/specifikationer/specifikationer/](http://www.perfag.dk/specifikationer/specifikationer/)  
Harmonized european standards [https://ec.europa.eu/growth/single-market/european-standards/harmonised-standards_en](https://ec.europa.eu/growth/single-market/european-standards/harmonised-standards_en)  
CCC standards [https://www.nist.gov/standardsgov/compliance-faqs-china-compulsory-certification-ccc-marking](https://www.nist.gov/standardsgov/compliance-faqs-china-compulsory-certification-ccc-marking)  

## Legislation
ROHS [https://www.rohsguide.com/](https://www.rohsguide.com/)  

## Other

Solder alloys [https://en.wikipedia.org/wiki/Solder_alloys](https://en.wikipedia.org/wiki/Solder_alloys)  
PCB substrates [https://www.essentracomponents.com/en-gb/news/guides/your-pcb-substrate-a-guide-to-materials](https://www.essentracomponents.com/en-gb/news/guides/your-pcb-substrate-a-guide-to-materials)  
Reflow, wave and hand soldering [https://youtu.be/saOHrw4ezGw](https://youtu.be/saOHrw4ezGw)  
Selective soldering [https://youtu.be/p-VImd2yW5s](https://youtu.be/p-VImd2yW5s)  
Pick and place machine [https://youtu.be/S8qkaTsr2_o](https://youtu.be/S8qkaTsr2_o)  
Odd shape pick and place machine [https://youtu.be/EUxBMUS45Kg](https://youtu.be/EUxBMUS45Kg)  
PCB factory [https://youtu.be/ljOoGyCso8s](https://youtu.be/ljOoGyCso8s)  
PCB Soldering Factory [https://youtu.be/24ehoo6RX8w](https://youtu.be/24ehoo6RX8w)  
Thermal profiling [https://youtu.be/XqOGCXw5QCw](https://youtu.be/XqOGCXw5QCw)  
Intermetallic layer [https://www.indium.com/blog/intermetallics-in-soldering.php](https://www.indium.com/blog/intermetallics-in-soldering.php)  
Materials lifetime [https://www.indium.com/blog/materials-lifetimes-a-modest-proposal.php](https://www.indium.com/blog/materials-lifetimes-a-modest-proposal.php)  
Link collection [https://blogs.mentor.com/jimmartens/blog/2019/06/21/ipc-7351-and-other-pcb-design-standards/](https://blogs.mentor.com/jimmartens/blog/2019/06/21/ipc-7351-and-other-pcb-design-standards/)
NISI orcad libraries (various footprints, padstacks and schematic symbols) [https://gitlab.com/npes/nisi-orcad-libraries](https://gitlab.com/npes/nisi-orcad-libraries)

## Books

Handbook of Electronic Assembly [https://www.smta.org/store/book_detail.cfm?book_id=436](https://www.smta.org/store/book_detail.cfm?book_id=436)  