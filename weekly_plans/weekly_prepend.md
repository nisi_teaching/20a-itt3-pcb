---
title: '20A ITT3 PCB'
subtitle: 'Weekly plans'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: '20A ITT3 PCB, weekly plans'
---


Introduction
====================

This document is a collection of weekly plans. It is based on the weekly plans in the administrative repository, and is updated automatically on change.

The sections describe the goals and programme for each week of the 20A ITT3 PCB elective course.

Most of the course is online due to Corona. However E-Lab is available for you to use and certain lectures will be with physical attendance.
