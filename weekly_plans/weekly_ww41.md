---
Week: 41
Content:  PCB Assembly
Material: See links in weekly plan
Initials: NISI
---

# Week 41 - PCB Assembly

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

Participate in Advanced PCB theory lecture  
Using Kicad

### Learning goals

The student can:

* Assemble a PCB according to schematic, bill of material and component placement docs
* Test a PCB according to test specifications

The student has knowledge about:

* The complexity in PCB's

## Deliverables

* PCB assembled
* PCB tested
* Gitlab log updated

## Schedule Tuesday

Lectures will be at zoom in room: **67929688905, password: 1234**

* 09:00 Introduction to the day + Q&A
* 09:30 Status
    * Students tells about the status of their work according to exercises
* 10:00 Work on deliverables and exercises
* 15:30 End of day

## Schedule Wednesday

Contact nisi if you have questions or need at meeting online

* 08:15 Work on deliverables and exercises
* 15:30 End of day

## Hands-on time

### Exercise 0 - Assemble produced PCB

Hopefully you received your PCB's and parts by now.

* Assemble your PCB according to the documentation you produced in week 39 (schematic, component placement, Bill of materials)
* Take pictures of the assembly proces for use in the poster exam

### Exercise 1 - Test assembled PCB's

* Test your assembled PCB according to the simulation you specified in week 37.
* Perform other relevant tests (input, output, testpoints, thermal etc.)

Document the test results in your gitlab log.  
It is important that you document your test in a uniform manner, that is using the same format for decimal results, screenshots of simulations etc. 
The test documentation will be a part of the poster you will make for the exam. 

## Comments
 
Exam is alarmingly close (October 21st!), if you are behind this is really the time to start catching up.