---
Week: 40
Content:  KiCad
Material: See links in weekly plan
Initials: NISI
---

# Week 40 - KiCad

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Circuit created using KiCad

### Learning goals

The student can:

* Use KiCad to design PCB's
* Implement design for manufacturing practices in PCB designs
* Use gained experience across EDA software suites

The student has knowledge about:

* Differences between EDA software suites

## Deliverables

* Pro's and con's about both KiCad and Orcad included in Gitlab log
* PCB rev 2 or other circuit designed in KiCad

## Schedule Tuesday

nisi is not available today

* 08:15 Work on deliverables and exercises
* 15:30 End of day

## Schedule Wednesday

Lectures will be at zoom in room: **67929688905, password: 1234**

* 10:00 Introduction to the day + Q&A
* 10:30 Status
    * Students tells about the status of their work according to exercises
* 11:00 KiCad presentation
* 15:30 End of day

## Hands-on time

### Exercise 0 - Manufacturing considerations

Based on the knowledge you now have about design for manufacturing, consider how you can change your design to improve the manufacturing proces.

Note your changes for use in exercise 1

### Exercise 1 - PCB revision 2 in KiCad

As an exercise in using KiCad, redraw your design in KiCad and include the changes gathered in exercise 0.  

My experience is that KiCad is a lot easier to use and has more parts and footprints included compared to OrCad, but you might have another opinion.  

Note the pro's and con's about both KiCad and Orcad in your gitlab log, use these in both your poster and pitch at the exam.

If you for some reason do not have to do a revision 2 of your PCB you should do a simple design in KiCad to get experience with the program suite.

Install KiCad from [www.kicad-pcb.org](http://www.kicad-pcb.org/)  
Instructions and other help at KiCad turorials and documentation [http://www.kicad-pcb.org/help/getting-started/](http://www.kicad-pcb.org/help/getting-started/)  
Other KiCad documentation [https://docs.kicad-pcb.org/](https://docs.kicad-pcb.org/)

## Comments
 
Based on the knowledge you know about the PCB design and manufacturing proces in Orcad, it should not be hard to pick up on KiCad using the tutorials and documentation.